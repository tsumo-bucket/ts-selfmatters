@extends('layouts.email-template')
@section('content')
  <h3>Hi {{$user['name']}},</h3>

  <p>
  	Your password has been changed by {{$logged['name']}}.
  </p>
 <p style="background:#ecf8ff;border:solid 2px #c7eaff;line-height:2;padding:14px">

  	<span>New Password: {{$password}}</span>
  	<br />
  	<span>Link: <a href="{{URL::to('/admin')}}"> {{URL::to('/admin')}} </a></span>
  
  </p>      
 @stop